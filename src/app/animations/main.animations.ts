import { animation, animate, keyframes, style, trigger, transition, query, stagger, useAnimation } from '@angular/animations';

export function slideIn(direction: 'left' | 'right') {
  return animation([
    animate('250ms ease-in', keyframes([

      style({
        opacity: 1,
        transform: 'translateX(0)'
      })
    ]))
  ]);
}

export const slideOut = animation([
  animate('100ms ease-in', keyframes([
    style({
      opacity: 0,
      transform: 'translateY(-100%)'
    })
  ]))
]);

export function globalStaggerAnimation(triggerElement: string, direction: 'left' | 'right') {
  return trigger(triggerElement, [
    transition('* <=> *', [
      query(
        ':enter',
        [
          style({
            opacity: 0,
            transform: direction === 'left' ? 'translateX(-100%)' : 'translateX(100%)'
          }),
          stagger(
            100,
            useAnimation(slideIn(direction))
          )
        ],
        { optional: true }
      ),
      query(
        ':leave',
        [
          style({
            opacity: 1,
          }),
          stagger(
            150,
            useAnimation(slideOut)
          )
        ],
        { optional: true }
      ),
    ])
  ]);
}
