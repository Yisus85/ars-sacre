import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeatroRoutingModule } from './teatro-routing.module';
import { HomeTeatroComponent } from '../../components/teatro/components/home-teatro/home-teatro.component';
import { TeatroComponentsModule } from './teatro-components.module';

@NgModule({
  imports: [
    CommonModule,
    TeatroRoutingModule,
    TeatroComponentsModule,
  ],
  declarations: []
})
export class TeatroModule { }
