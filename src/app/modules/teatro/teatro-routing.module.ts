import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeTeatroComponent } from '../../components/teatro/components/home-teatro/home-teatro.component';

const routes: Routes = [
  { path: '', component: HomeTeatroComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeatroRoutingModule { }
