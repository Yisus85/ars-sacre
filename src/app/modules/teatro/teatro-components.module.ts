import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../../modules/shared/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeTeatroComponent } from '../../components/teatro/components/home-teatro/home-teatro.component';

@NgModule({
  declarations: [
    HomeTeatroComponent,
  ],
  exports: [
    HomeTeatroComponent,
    CommonModule,
    FormsModule,
  ],
  imports: [
    MaterialModule,
  ],
})
export class TeatroComponentsModule {
}
