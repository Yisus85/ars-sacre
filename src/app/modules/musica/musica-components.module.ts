import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material.module';
import { HomeMusicaComponent } from '../../components/musica/components/home-musica/home-musica.component';
import { MisaComponent } from '../../components/musica/components/misa/misa.component';
import { SalmosComponent } from '../../components/musica/components/salmos/salmos.component';
import { RosarioComponent } from '../../components/musica/components/rosario/rosario.component';
import { NavidadComponent } from '../../components/musica/components/navidad/navidad.component';
import { CotidianaComponent } from '../../components/musica/components/misa/components/cotidiana/cotidiana.component';
import { DominicalComponent } from '../../components/musica/components/misa/components/dominical/dominical.component';
import { EntradaComponent } from '../../components/musica/components/misa/components/entrada/entrada.component';
import { AleluyaComponent } from '../../components/musica/components/misa/components/aleluya/aleluya.component';
import { OfertorioComponent } from '../../components/musica/components/misa/components/ofertorio/ofertorio.component';
import { PadrenuestroComponent } from '../../components/musica/components/misa/components/padrenuestro/padrenuestro.component';
import { ComunionComponent } from '../../components/musica/components/misa/components/comunion/comunion.component';
import { SalidaComponent } from '../../components/musica/components/misa/components/salida/salida.component';
import { CicloAComponent } from '../../components/musica/components/salmos/components/ciclo-a/ciclo-a.component';
import { CicloBComponent } from '../../components/musica/components/salmos/components/ciclo-b/ciclo-b.component';
import { CicloCComponent } from '../../components/musica/components/salmos/components/ciclo-c/ciclo-c.component';
import { GloriaComponent } from '../../components/musica/components/rosario/components/gloria/gloria.component';
import { PesameComponent } from '../../components/musica/components/rosario/components/pesame/pesame.component';
import { JesucristoComponent } from '../../components/musica/components/rosario/components/jesucristo/jesucristo.component';
import { VillancicosComponent } from '../../components/musica/components/navidad/components/villancicos/villancicos.component';
import { CancionesComponent } from '../../components/musica/components/navidad/components/canciones/canciones.component';
import { RouterModule } from '@angular/router';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { PartituraComponent } from '../../../app/components/shared/partitura/partitura.component';
import { ListaPartiturasComponent } from '../../../app/components/shared/lista-partituras/lista-partituras.component';
import { ConstruccionComponent } from '../../../app/components/shared/construccion/construccion.component';
import { TopPublilcidadComponent } from '../../../app/components/shared/topPartituraPublicidad/top-Partitura-publicidad.component';
import { DialogComponent } from '../../../app/components/shared/dialog/dialog.component';
import { ErrorComponent } from '../../../app/components/shared/error/error.component';

@NgModule({
  declarations: [
    HomeMusicaComponent,
    SalmosComponent,
    RosarioComponent,
    NavidadComponent,
    MisaComponent,
    CotidianaComponent,
    DominicalComponent,
    EntradaComponent,
    AleluyaComponent,
    OfertorioComponent,
    PadrenuestroComponent,
    ComunionComponent,
    SalidaComponent,
    CicloAComponent,
    CicloBComponent,
    CicloCComponent,
    GloriaComponent,
    PesameComponent,
    JesucristoComponent,
    VillancicosComponent,
    CancionesComponent,
    PartituraComponent,
    ListaPartiturasComponent,
    ConstruccionComponent,
    TopPublilcidadComponent,
    DialogComponent,
    ErrorComponent,
  ],
  exports: [
    HomeMusicaComponent,
    SalmosComponent,
    RosarioComponent,
    NavidadComponent,
    MisaComponent,
    CommonModule,
    FormsModule,
    CotidianaComponent,
    DominicalComponent,
    EntradaComponent,
    AleluyaComponent,
    OfertorioComponent,
    PadrenuestroComponent,
    ComunionComponent,
    SalidaComponent,
    CicloAComponent,
    CicloBComponent,
    CicloCComponent,
    GloriaComponent,
    PesameComponent,
    JesucristoComponent,
    VillancicosComponent,
    CancionesComponent,
    PartituraComponent,
    ListaPartiturasComponent,
    ConstruccionComponent,
    TopPublilcidadComponent,
    ErrorComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    PdfViewerModule,
  ],
  entryComponents: [
    DialogComponent
  ]
})
export class MusicaComponentsModule {
}
