import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicaComponentsModule } from './musica-components.module';
import { MusicaRoutingModule } from './musica-routing.module';

@NgModule({
  imports: [
    CommonModule,
    MusicaRoutingModule,
    MusicaComponentsModule,
  ],
  declarations: [
  ]
})
export class MusicaModule { }
