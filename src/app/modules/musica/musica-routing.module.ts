import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MisaComponent } from '../../components/musica/components/misa/misa.component';
import { SalmosComponent } from '../../components/musica/components/salmos/salmos.component';
import { RosarioComponent } from '../../components/musica/components/rosario/rosario.component';
import { NavidadComponent } from '../../components/musica/components/navidad/navidad.component';
import { HomeMusicaComponent } from '../../components/musica/components/home-musica/home-musica.component';
import { CotidianaComponent } from '../../components/musica/components/misa/components/cotidiana/cotidiana.component';
import { DominicalComponent } from '../../components/musica/components/misa/components/dominical/dominical.component';
import { EntradaComponent } from '../../components/musica/components/misa/components/entrada/entrada.component';
import { AleluyaComponent } from '../../components/musica/components/misa/components/aleluya/aleluya.component';
import { OfertorioComponent } from '../../components/musica/components/misa/components/ofertorio/ofertorio.component';
import { PadrenuestroComponent } from '../../components/musica/components/misa/components/padrenuestro/padrenuestro.component';
import { ComunionComponent } from '../../components/musica/components/misa/components/comunion/comunion.component';
import { SalidaComponent } from '../../components/musica/components/misa/components/salida/salida.component';
import { CicloAComponent } from '../../components/musica/components/salmos/components/ciclo-a/ciclo-a.component';
import { CicloBComponent } from '../../components/musica/components/salmos/components/ciclo-b/ciclo-b.component';
import { CicloCComponent } from '../../components/musica/components/salmos/components/ciclo-c/ciclo-c.component';
import { GloriaComponent } from '../../components/musica/components/rosario/components/gloria/gloria.component';
import { PesameComponent } from '../../components/musica/components/rosario/components/pesame/pesame.component';
import { JesucristoComponent } from '../../components/musica/components/rosario/components/jesucristo/jesucristo.component';
import { VillancicosComponent } from '../../components/musica/components/navidad/components/villancicos/villancicos.component';
import { CancionesComponent } from '../../components/musica/components/navidad/components/canciones/canciones.component';
import { PartituraComponent } from '../../../app/components/shared/partitura/partitura.component';

const routes: Routes = [
  { path: '', component: HomeMusicaComponent },
  { path: 'partitura', component: PartituraComponent },
  { path: 'misa', component: MisaComponent },
  { path: 'misa/cotidiana', component: CotidianaComponent },
  { path: 'misa/dominical', component: DominicalComponent },
  { path: 'misa/entrada', component: EntradaComponent },
  { path: 'misa/aleluya', component: AleluyaComponent },
  { path: 'misa/ofertorio', component: OfertorioComponent },
  { path: 'misa/padre-nuestro', component: PadrenuestroComponent },
  { path: 'misa/comunion', component: ComunionComponent },
  { path: 'misa/salida', component: SalidaComponent },
  { path: 'salmos', component: SalmosComponent },
  { path: 'salmos/ciclo-a', component: CicloAComponent },
  { path: 'salmos/ciclo-b', component: CicloBComponent },
  { path: 'salmos/ciclo-c', component: CicloCComponent },
  { path: 'rosario', component: RosarioComponent },
  { path: 'rosario/gloria', component: GloriaComponent },
  { path: 'rosario/pesame', component: PesameComponent },
  { path: 'rosario/jesucristo', component: JesucristoComponent },
  { path: 'navidad', component: NavidadComponent },
  { path: 'navidad/villancicos', component: VillancicosComponent },
  { path: 'navidad/canciones', component: CancionesComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MusicaRoutingModule { }
