import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'canto-liturgico-y-religioso', loadChildren: './modules/musica/musica.module#MusicaModule' },
  { path: 'teatro-religioso', loadChildren: './modules/teatro/teatro.module#TeatroModule' },
  { path: '**', redirectTo: '/home' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
