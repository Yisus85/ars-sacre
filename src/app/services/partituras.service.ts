import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, catchError, map, shareReplay } from 'rxjs/operators';
import { throwError, of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartiturasService {
  private graphQLAPI = 'https://ars-sacre-server.herokuapp.com/api/1.0/graphql';
  private _type: string;
  private graphQLQuery = {
    query: `
  query {
    ${this._type}{
      partituraNumero
      titulo
      path
    }
  }
  `};

  public partituras$: Observable<IPartitura[]>;
  public totalParituras$: Observable<any> = this.getTotalParituras();

  constructor(private _http: HttpClient) {
  }

  set type(type) {
    this._type = type;
    this.partituras$ = this._http.post<GQLResponse<IPartitura[]>>(this.graphQLAPI, {
      query: `
    query {
      ${this._type}{
        partituraNumero
        titulo
        path
      }
    }
    `}).pipe(
        map(({ data }) => data[`${this._type}`].sort((a, b) => a.partituraNumero - b.partituraNumero)),
        catchError(this.handleError)
      );
  }

  get type() {
    return this._type;
  }

  getPartiturasFromServer(path) {
    const url = path;
    return this._http.post(`https://ars-sacre-server.herokuapp.com/api/1.0/partituras`, { url }, { 'responseType': 'arraybuffer' });
  }

  getTotalParituras() {
    return this._http.post('https://ars-sacre-server.herokuapp.com/api/1.0/graphql', {
      query: `
      query {
        totalCount
      }
      `
    }).pipe(
      shareReplay(1),
      catchError(this.handleError)
    );
  }

  private handleError(err: any) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage: string;

    if (err.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Backend returned code ${err.status}: ${err.statusText}`;
    }
    return throwError(errorMessage);
  }
}
