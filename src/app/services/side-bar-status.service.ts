import { Injectable, OnDestroy } from '@angular/core';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { Router, NavigationEnd } from '@angular/router';
import { Event as NavigationEvent } from '@angular/router';

interface SideBarStatusInterface {
  visible: boolean;
}

@Injectable()
export class SideBarStatus implements OnDestroy {
  private sideBarStatus: BehaviorSubject<SideBarStatusInterface> = new BehaviorSubject<SideBarStatusInterface>({ visible: false });
  public currentSideBarStatus: Observable<SideBarStatusInterface> = this.sideBarStatus.asObservable();
  public urlSubscription: Subscription;
  private musicaData = {
    displayName: 'El Canto Litúrgico y Religioso',
    iconName: 'audiotrack',
    title: 'MUSICA',
    children: [
      {
        displayName: 'La Santa Misa',
        iconName: 'person',
        route: 'canto-liturgico-y-religioso/misa',
        title: 'MISA',
        children: [
          // tslint:disable-next-line:max-line-length
          { displayName: 'Regreso a Canto Litúrgico y Religioso', iconName: 'person', route: 'canto-liturgico-y-religioso', disabled: true },
          { displayName: 'Misa Cotidiana', iconName: 'person', route: 'canto-liturgico-y-religioso/misa/cotidiana', disabled: true },
          { displayName: 'Misa Dominical', iconName: 'person', route: 'canto-liturgico-y-religioso/misa/dominical', disabled: true },
          { displayName: 'Entrada', iconName: 'person', route: 'canto-liturgico-y-religioso/misa/entrada', disabled: true },
          { displayName: 'Aleluya', iconName: 'person', route: 'canto-liturgico-y-religioso/misa/aleluya', disabled: true },
          { displayName: 'Ofertorio', iconName: 'person', route: 'canto-liturgico-y-religioso/misa/ofertorio', disabled: true },
          { displayName: 'Padre Nuestro', iconName: 'person', route: 'canto-liturgico-y-religioso/misa/padre-nuestro', disabled: true },
          { displayName: 'Comunión', iconName: 'person', route: 'canto-liturgico-y-religioso/misa/comunion', disabled: true },
          { displayName: 'Salida', iconName: 'person', route: 'canto-liturgico-y-religioso/misa/salida', disabled: true },
        ]
      },
      {
        displayName: 'Salmos',
        iconName: 'group',
        route: 'canto-liturgico-y-religioso/salmos',
        title: 'SALMOS',
        children: [
          // tslint:disable-next-line:max-line-length
          { displayName: 'Regreso a Canto Litúrgico y Religioso', iconName: 'person', route: 'canto-liturgico-y-religioso', disabled: true },
          { displayName: 'Ciclo A', iconName: 'person', route: 'canto-liturgico-y-religioso/salmos/ciclo-a', disabled: true },
          { displayName: 'Ciclo B', iconName: 'person', route: 'canto-liturgico-y-religioso/salmos/ciclo-b', disabled: true },
          { displayName: 'Ciclo C', iconName: 'person', route: 'canto-liturgico-y-religioso/salmos/ciclo-c', disabled: true },
        ]
      },
      {
        displayName: 'El Santo Rosario',
        iconName: 'group',
        route: 'canto-liturgico-y-religioso/rosario',
        title: 'ROSARIO',
        children: [
          // tslint:disable-next-line:max-line-length
          { displayName: 'Regreso a Canto Litúrgico y Religioso', iconName: 'person', route: 'canto-liturgico-y-religioso', disabled: true },
          {
            displayName: 'Cantos y motetes de Gloria',
            iconName: 'person',
            route: 'canto-liturgico-y-religioso/rosario/gloria',
            disabled: true
          },
          {
            displayName: 'Cantos y motetes de Pésame',
            iconName: 'person',
            route: 'canto-liturgico-y-religioso/rosario/pesame',
            disabled: true
          },
          {
            displayName: 'Cantos y Motetes a N. S. Jesucristo',
            iconName: 'person',
            route: 'canto-liturgico-y-religioso/rosario/jesucristo',
            disabled: true
          },
        ]
      },
      {
        displayName: 'Navidad',
        iconName: 'group',
        route: 'canto-liturgico-y-religioso/navidad',
        title: 'VILLANCICOS',
        children: [
          { displayName: 'Regreso a Navidad', iconName: 'person', route: 'canto-liturgico-y-religioso/navidad', disabled: true },
          { displayName: 'Villancicos', iconName: 'person', route: 'canto-liturgico-y-religioso/navidad/villancicos', disabled: true },
          { displayName: 'Canciones', iconName: 'person', route: 'canto-liturgico-y-religioso/navidad/canciones', disabled: true },
        ]
      },
    ],
  } as NavItem;
  private teatroData = {
    displayName: 'Teatro Religioso',
    iconName: 'mood',
    title: 'TEATRO',
    disabled: true,
  } as NavItem;

  public navDataStatus: BehaviorSubject<{ navData: NavItem }> = new BehaviorSubject<{ navData: NavItem }>({ navData: {} as NavItem });

  constructor(
    private _router: Router
  ) {

    this.urlSubscription = this._router.events.subscribe(
      (e: NavigationEvent) => {
        if (e instanceof NavigationEnd) {
          const { url } = e;

          if (url.includes('/misa/')) {
            this.navDataStatus.next({ navData: this.musicaData.children[0] });
          } else if (url.includes('/salmos/')) {
            this.navDataStatus.next({ navData: this.musicaData.children[1] });
          } else if (url.includes('/rosario/')) {
            this.navDataStatus.next({ navData: this.musicaData.children[2] });
          } else if (url.includes('/navidad/')) {
            this.navDataStatus.next({ navData: this.musicaData.children[3] });
          }
          switch (url) {
            case '/':
              this.sideBarStatus.next({ visible: false });
              break;
            case '/home':
              this.sideBarStatus.next({ visible: false });
              break;
            case '/teatro-religioso':
              this.sideBarStatus.next({ visible: false });
              this.navDataStatus.next({ navData: this.teatroData });
              break;
            case '/canto-liturgico-y-religioso':
              this.sideBarStatus.next({ visible: false });
              this.navDataStatus.next({ navData: this.musicaData });
              break;
            case '/canto-liturgico-y-religioso/misa':
              this.sideBarStatus.next({ visible: false });
              this.navDataStatus.next({ navData: this.musicaData.children[0] });
              break;
            case '/canto-liturgico-y-religioso/salmos':
              this.sideBarStatus.next({ visible: false });
              this.navDataStatus.next({ navData: this.musicaData.children[1] });
              break;
            case '/canto-liturgico-y-religioso/rosario':
              this.sideBarStatus.next({ visible: false });
              this.navDataStatus.next({ navData: this.musicaData.children[2] });
              break;
            case '/canto-liturgico-y-religioso/navidad':
              this.sideBarStatus.next({ visible: false });
              this.navDataStatus.next({ navData: this.musicaData.children[3] });
              break;
            default:
              this.sideBarStatus.next({ visible: false });
              return;
          }
        }
      }
    );
  }

  ngOnDestroy() {
    if (this.urlSubscription) {
      this.urlSubscription.unsubscribe();
    }
  }

  showHideSideNav(newStatus: SideBarStatusInterface): void {
    this.sideBarStatus.next(newStatus);
  }
}
