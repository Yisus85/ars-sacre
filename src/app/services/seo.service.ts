import { Injectable } from '@angular/core';
import { Meta } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class SeoService {
  constructor(private meta: Meta) { }

  createTags(config: SEOConfigObject) {
    config = {
      author: `David D' Leon`,
      description: 'Partituras de musica catolica',
      image: 'https://www.ars-sacre.com/assets/images/imagenPrincipal-small.jpg',
      slug: '',
      title: 'Ars-Sacre. Partituras de música católica',
      ...config,
    };

    this.meta.updateTag({ name: 'twitter:card', content: 'summary' });
    this.meta.updateTag({ name: 'twitter:site', content: '@ars_sacre' });
    this.meta.updateTag({ name: 'twitter:creator', content: config.author });
    this.meta.updateTag({ name: 'twitter:title', content: config.title });
    this.meta.updateTag({ name: 'twitter:description', content: config.description });
    this.meta.updateTag({ name: 'twitter:image', content: config.image });

    this.meta.updateTag({ name: 'og:type', content: 'website' });
    this.meta.updateTag({ name: 'og:site_name', content: 'ars-sacre.com' });
    this.meta.updateTag({ name: 'og:title', content: config.title });
    this.meta.updateTag({ name: 'og:description', content: config.description });
    this.meta.updateTag({ name: 'og:image', content: config.image });
    this.meta.updateTag({ name: 'og:url', content: `https://www.ars-sacre.com/${config.slug}` });
    this.meta.updateTag({ name: 'og:video', content: ' https://youtu.be/PAGp_ZbdvFQt'});
  }
}
