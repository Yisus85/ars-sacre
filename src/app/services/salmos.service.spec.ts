import { TestBed } from '@angular/core/testing';

import { PartiturasService } from './partituras.service';

describe('SalmosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PartiturasService = TestBed.get(PartiturasService);
    expect(service).toBeTruthy();
  });
});
