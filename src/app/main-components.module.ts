import { NgModule } from '@angular/core';
import { NavBarComponent } from './components/shared/nav-bar/nav-bar.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from './modules/shared/material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { SideNavComponent } from './components/shared/side-nav/side-nav.component';
import { MainContainerComponent } from './components/main-container/main-container.component';
import { AppRoutingModule } from './app-routing.module';
import { SideBarStatus } from './services/side-bar-status.service';
import { BioComponent } from './components/shared/bio/bio.component';
import { MusicaModule } from './modules/musica/musica.module';
import { SeoService } from './services/seo.service';
// import { MusicNotesComponent } from './components/shared/musicNotes/music-notes.component';

@NgModule({
  providers: [SideBarStatus, SeoService],
  declarations: [
    NavBarComponent,
    HomeComponent,
    FooterComponent,
    SideNavComponent,
    MainContainerComponent,
    BioComponent,
    // MusicNotesComponent,
  ],
  exports: [
    NavBarComponent,
    HomeComponent,
    FooterComponent,
    SideNavComponent,
    CommonModule,
    FormsModule,
    MainContainerComponent,
    BioComponent,
    // MusicNotesComponent,
  ],
  imports: [
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MusicaModule,
  ],
})
export class MainComponentsModule {
}
