declare module '*.scss'

interface SEOConfigObject{
  title: string;
  description: string;
  image?: string;
  author: string;
  slug: string;
}