import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Router, NavigationEnd, OutletContext } from '@angular/router';
import { Subscription } from 'rxjs';
import { SideBarStatus } from '../../../app/services/side-bar-status.service';

@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.scss']
})
export class MainContainerComponent implements OnInit, OnDestroy {
  public sideNavStatus: boolean;
  private sideNavSubscription: Subscription;
  private routerSubscription: Subscription;
  @ViewChild('routerOutlet') outlet: ElementRef;

  constructor(
    private _sideBarStatus: SideBarStatus,
    private _router: Router,
  ) { }

  ngOnInit() {
    this.sideNavSubscription = this._sideBarStatus.currentSideBarStatus.subscribe(
      ({ visible }) => {
        this.sideNavStatus = visible;
      }
    );
    this.routerSubscription = this._router.events.subscribe(
      (event) => {
        if (event instanceof NavigationEnd) {
          this.outlet.nativeElement.scrollIntoView(0, 0);
        }
      }
    );
  }

  backdropClicked() {
    this._sideBarStatus.showHideSideNav({ visible: false });
  }

  ngOnDestroy() {
    if (this.sideNavSubscription) {
      this.sideNavSubscription.unsubscribe();
    }
    if (this.routerSubscription) {
      this.routerSubscription.unsubscribe();
    }
  }

}
