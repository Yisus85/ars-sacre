import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTeatroComponent } from './home-teatro.component';

describe('HomeTeatroComponent', () => {
  let component: HomeTeatroComponent;
  let fixture: ComponentFixture<HomeTeatroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeTeatroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTeatroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
