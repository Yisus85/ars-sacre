import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {

  public videoHasEnded = false;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.videoHasEnded = true;
    }, 5500);
  }

}
