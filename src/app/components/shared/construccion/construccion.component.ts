import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-construccion',
  templateUrl: './construccion.component.html',
  styleUrls: ['./construccion.component.scss']
})
export class ConstruccionComponent implements OnInit {

  public videoHasEnded = false;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.videoHasEnded = true;
    }, 15500);
  }

}
