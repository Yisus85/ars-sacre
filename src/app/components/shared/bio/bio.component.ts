import { Component, ViewChild, ElementRef, NgZone, Renderer2, AfterViewInit, OnDestroy } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
@Component({
  selector: 'app-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.scss']
})
export class BioComponent implements AfterViewInit, OnDestroy {

  @ViewChild('imageContainer') imageContainer: ElementRef;
  @ViewChild('imageReference') imageReference: ElementRef;
  private observer: IntersectionObserver | null;
  private zone: NgZone;
  public imageSrc = 'assets/images/papaleon.jpg';

  constructor(
    private renderer: Renderer2,
    private deviceDetector: DeviceDetectorService,
    zone: NgZone,
  ) {
    this.zone = zone;
  }

  checkImage(source) {
    if (document) {
      const img = new Image();
      img.onload = () => {
        this.imageSrc = source;
        this.renderer.setProperty(this.imageReference.nativeElement, 'src', this.imageSrc);
      };
      img.onerror = () => {
        this.imageSrc = this.setImageFormat();
        this.renderer.setProperty(this.imageReference.nativeElement, 'src', this.imageSrc);
      };
      img.src = source;
    }
  }

  setImageFormat() {
    const isDesktop = this.deviceDetector.isDesktop();
    const browser = this.deviceDetector.browser.toLowerCase();
    let theSource: string;
    switch (browser) {
      case 'safari':
        if (parseFloat(this.deviceDetector.browser_version) >= 12.1) {
          theSource = 'assets/images/papaleon.jp2';
        } else {
          theSource = 'assets/images/papaleon.jpg';
        }
        break;

      case 'ie' || 'ms-edge':
        if (parseFloat(this.deviceDetector.browser_version) >= 9.1) {
          theSource = 'assets/images/papaleon.jxr';
        } else {
          theSource = 'assets/images/papaleon.jpg';
        }
        break;

      default:
        theSource = 'assets/images/papaleon.jpg';
    }
    return theSource;
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(
      () => {
        if (!window['IntersectionObserver']) {
          setTimeout(() => {
            const theSrc: string = this.imageReference.nativeElement.src;
            if (!theSrc.includes(this.imageSrc)) {
              this.checkImage(this.imageSrc);
            }
          }, 3000);
          return;
        } else {
          this.observer = new IntersectionObserver((e: any) => {
            if (e[0].isIntersecting) {
              const theSrc: string = this.imageReference.nativeElement.src;
              if (!theSrc.includes(this.imageSrc)) {
                this.checkImage(this.imageSrc);
              }
            }
          });
          this.observer.observe(this.imageContainer.nativeElement);
        }
      }
    );
  }

  ngOnDestroy() {
    if (this.observer) {
      this.observer.disconnect();
      this.observer = null;
    }
  }

}
