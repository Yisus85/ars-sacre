import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { SideBarStatus } from '../../../../app/services/side-bar-status.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit, OnDestroy {
  public routeSubscription: Subscription;
  public _url = '';
  public sideNavTitle = '';
  public sideNavItems = [];
  public navItems;

  constructor(
    private _navStatusService: SideBarStatus,
    private _router: Router
  ) { }

  ngOnInit() {
    this.routeSubscription = this._navStatusService.navDataStatus.subscribe(
      (data) => {
        this.navItems = data.navData;
      }
    );
  }

  ngOnDestroy() {
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
  }

  goToUrl(url): void {
    this._router.navigateByUrl(`/${url}`);
  }

}
