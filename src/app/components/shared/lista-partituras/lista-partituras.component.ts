import { Component, ViewChild, ElementRef, HostListener, AfterViewInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, combineLatest, EMPTY, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { PartiturasService } from '../../../../app/services/partituras.service';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material/icon';
import { globalStaggerAnimation } from '../../../../app/animations/main.animations';

@Component({
  selector: 'app-lista-partituras',
  templateUrl: './lista-partituras.component.html',
  styleUrls: ['./lista-partituras.component.scss'],
  animations: [globalStaggerAnimation('parituraContainer', 'left')]
})
export class ListaPartiturasComponent implements AfterViewInit {

  constructor(
    private _partiturasService: PartiturasService,
    private router: Router,
    private domSanitizaer: DomSanitizer,
    private matIconRegistry: MatIconRegistry,
  ) {
    this.matIconRegistry.addSvgIcon('audiotrack', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/audiotrack.svg'));
  }
  public currentPdf;
  public isLoading = true;
  public divWidth = 0;
  public dataSource;
  public displayedColumns = ['numeroLista', 'tituloSalmo', 'numeroSalmo', 'descargar'];
  @Input() query: string;
  @Input() navigateTo: string;
  @Input() headerTitulo: string;
  @Input() tipoPartitura: string;
  @ViewChild('headerContainer') headerContainer: ElementRef;
  @ViewChild('publicidad') publicidad: ElementRef;
  @ViewChild('publicidad2') publicidad2: ElementRef;
  public partituras$: Observable<IPartitura[]>;
  private filterSubject = new BehaviorSubject<string>('');
  public filterAction$: Observable<string> = this.filterSubject.asObservable();
  public error = false;

  @HostListener('window:resize') onResize() {
    if (this.headerContainer) {
      this.divWidth = this.headerContainer.nativeElement.clientWidth;
      this.setHeightPublicidad(this.divWidth);
    }
  }

  private setHeightPublicidad(_width: number) {
    if (_width <= 720) {
      this.publicidad.nativeElement.style.height = '60px';
      this.publicidad2.nativeElement.style.height = '60px';
    } else {
      this.publicidad.nativeElement.style.height = '90px';
      this.publicidad2.nativeElement.style.height = '90px';
    }
  }

  ngAfterViewInit() {
    setTimeout(
      () => {
        this.divWidth = this.headerContainer.nativeElement.clientWidth;
        this.setHeightPublicidad(this.divWidth);
        this._partiturasService.type = this.query;
        this.partituras$ = combineLatest([
          this._partiturasService.partituras$,
          this.filterAction$
        ]).pipe(
          map(({ 0: info, 1: filter }: { 0: IPartitura[], 1: string }) => {
            this.isLoading = false;
            // tslint:disable-next-line:max-line-length
            return filter === '' ? info : info.filter(p => p.titulo.toUpperCase().includes(filter.toUpperCase()) || `${p.partituraNumero}`.includes(filter));
          }),
          catchError(err => {
            console.log(err);
            this.isLoading = false;
            this.error = true;
            return EMPTY;
          }));
      });
  }

  public getPartitura(path: string, titulo: string) {
    const _path = btoa(path);
    this.router.navigate([`${this.navigateTo}`], { queryParams: { id: _path, titulo } });
  }

  applyFilter(filterValue: string) {
    this.filterSubject.next(filterValue);
  }
}
