import { Component, OnInit, OnDestroy, HostListener, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { PartiturasService } from '../../../../app/services/partituras.service';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-partitura',
  templateUrl: './partitura.component.html',
  styleUrls: ['./partitura.component.scss']
})
export class PartituraComponent implements OnInit, OnDestroy, AfterViewInit {

  public zoom = 1;
  public pdfPartitura;
  private routerSubscription: Subscription;
  private partituraSubscription: Subscription;
  private dialogSubscription: Subscription;
  public divWidth: number;
  public isLoading = true;
  public titulo: string;

  @ViewChild('publicidad') mainContainer: ElementRef;
  @HostListener('window:resize') onResize() {
    if (this.mainContainer) {
      this.divWidth = this.mainContainer.nativeElement.clientWidth;
      this.zoom = this.setZoom(this.divWidth);
    }
  }

  constructor(private dialog: MatDialog, private route: ActivatedRoute, private _salmosService: PartiturasService) {

  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.divWidth = this.mainContainer.nativeElement.clientWidth;
      this.zoom = this.setZoom(this.divWidth);
    });
  }

  private setZoom(width: number) {
    if (width <= 320) {
      return 0.4;
    }
    if (width > 320 && width <= 400) {
      return 0.45;
    }
    if (width > 400 && width <= 500) {
      return 0.6;
    }
    if (width > 500 && width <= 720) {
      return 0.8;
    }
    return 1;
  }

  ngOnInit() {
    this.routerSubscription = this.route.queryParams.subscribe(
      ({ id, titulo }) => {
        this.partituraSubscription = this._salmosService.getPartiturasFromServer(id).subscribe(
          (pdf) => {
            const blob = new Blob([pdf], { type: 'application/pdf' });
            const objectUrl = URL.createObjectURL(blob);
            this.pdfPartitura = objectUrl;
            this.titulo = `${titulo}.pdf`;
            this.isLoading = false;
          }
        );
      }
    );
  }

  ngOnDestroy() {
    if (this.routerSubscription) {
      this.routerSubscription.unsubscribe();
    }
    if (this.partituraSubscription) {
      this.partituraSubscription.unsubscribe();
    }
    if (this.dialogSubscription) {
      this.dialogSubscription.unsubscribe();
    }
  }

  public incZoom() {
    this.increaseZoom();
  }

  public decZoom() {
    this.decreaseZoom();
  }

  public descargar() {
    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
      // IE11 and Edge
      window.navigator.msSaveOrOpenBlob(this.pdfPartitura, this.titulo);
    } else {
      // Chrome, Safari, Firefox, Opera
      const url = this.pdfPartitura;
      this.openLink(url);
      // Remove the link when done
      setTimeout(function () {
        window.URL.revokeObjectURL(url);
      }, 0);
    }
  }

  public openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '320px',
      height: '400px',
      data: { test: 'this.name' }
    });

    this.dialogSubscription = dialogRef.afterClosed().subscribe(result => {
      if (result === 'descargar') {
        this.descargar();
      }
    });
  }

  private openLink(url: string) {
    const a = document.createElement('a');
    // Firefox requires the link to be in the body
    document.body.appendChild(a);
    a.style.display = 'none';
    a.href = url;
    a.download = this.titulo;
    a.click();
    // Remove the link when done
    document.body.removeChild(a);
  }

  private increaseZoom() {
    if (this.zoom >= 1.5) {
      return;
    }
    this.zoom = (Math.floor(this.zoom * 100) + 5) / 100;
  }

  private decreaseZoom() {
    if (this.zoom <= 0.3) {
      return;
    }
    this.zoom = (Math.floor(this.zoom * 100) - 5) / 100;
  }
}
