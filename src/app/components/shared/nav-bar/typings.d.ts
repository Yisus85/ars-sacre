interface TemaPrincipal{
    titulo: string;
    secciones: Array<SeccionInterface>;
    icono: string;
    MenuID: string;
}

interface SeccionInterface{
    titulo: string;
    subtemas: string[];
    subMenuID: string;
}

interface NavItem {
    displayName: string;
    disabled?: boolean;
    iconName: string;
    route?: string;
    children?: NavItem[];
    title: string;
}

interface IPartitura{
    partituraNumero: number;
    titulo: string;
    path: string;
}

interface GQLResponse<T>{
    data: T;
}