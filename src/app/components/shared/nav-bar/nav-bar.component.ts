import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, NavigationEnd } from '@angular/router';
import { SideBarStatus } from '../../../../app/services/side-bar-status.service';
import { Subscription } from 'rxjs';
import { Event as NavigationEvent } from '@angular/router';
@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class NavBarComponent implements OnInit, OnDestroy {
  public sideNavSubscription: Subscription;
  public routerUrlSubscription: Subscription;
  public sideNavStatus = false;
  public hideMenu = 'hidden';

  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizaer: DomSanitizer,
    private router: Router,
    private _sideBarStatus: SideBarStatus,
  ) {
    this.matIconRegistry.addSvgIcon('misa', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/church.svg'));
    this.matIconRegistry.addSvgIcon('salmos', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/bible.svg'));
    this.matIconRegistry.addSvgIcon('rosario', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/nun.svg'));
    this.matIconRegistry.addSvgIcon('navidad', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/ball.svg'));
    this.matIconRegistry.addSvgIcon('teatro', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/theater.svg'));
    this.matIconRegistry.addSvgIcon('orquesta', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/orquesta.svg'));
    this.matIconRegistry.addSvgIcon('menu', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/menu.svg'));
    this.matIconRegistry.addSvgIcon('home', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/home.svg'));
    this.matIconRegistry.addSvgIcon('audiotrack', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/audiotrack.svg'));
  }

  ngOnInit() {
    this.sideNavSubscription = this._sideBarStatus.currentSideBarStatus.subscribe(
      ({ visible }) => {
        this.sideNavStatus = visible;
      }
    );
    this.routerUrlSubscription = this.router.events.subscribe(
      (e: NavigationEvent) => {
        if (e instanceof NavigationEnd) {
          const { url } = e;
          if ((url === '/home') || (url === '/teatro-religioso')) {
            this.hideMenu = 'hidden';
          } else {
            this.hideMenu = 'visible';
          }
        }
      }
    );
  }

  ngOnDestroy() {
    if (this.sideNavSubscription) {
      this.sideNavSubscription.unsubscribe();
    }
    if (this.routerUrlSubscription) {
      this.routerUrlSubscription.unsubscribe();
    }
  }

  goToUrl(url): void {
    this.router.navigateByUrl(`/${url}`);
  }

  toggleSideNav() {
    this._sideBarStatus.showHideSideNav({ visible: !this.sideNavStatus });
  }
}
