import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pesame',
  template: `<app-lista-partituras
    [query]='_query'
    [navigateTo]='_navigateTo'
    [headerTitulo]='_titulo'
    [tipoPartitura]="_partitura"
  ></app-lista-partituras>`,
})
export class PesameComponent {
  public _query = 'pesame';
  public _navigateTo = 'canto-liturgico-y-religioso/partitura';
  public _titulo = 'CANTOS DE PESAME';
  public _partitura = 'CANTOS DE PESAME';
}
