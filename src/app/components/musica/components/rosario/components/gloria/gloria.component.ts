import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gloria',
  template: `<app-lista-partituras
    [query]='gloriaQuery'
    [navigateTo]='navigateToGloria'
    [headerTitulo]='titulo'
    [tipoPartitura]="tipoPartitura"
  ></app-lista-partituras>`,
})
export class GloriaComponent {
  public gloriaQuery = 'gloria';
  public navigateToGloria = 'canto-liturgico-y-religioso/partitura';
  public titulo = 'CANTOS DE GLORIA';
  public tipoPartitura = 'MOTETES DE GLORIA';
}

