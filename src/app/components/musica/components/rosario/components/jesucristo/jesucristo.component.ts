import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jesucristo',
  template: `<app-lista-partituras
    [query]='_query'
    [navigateTo]='_navigateTo'
    [headerTitulo]='_titulo'
    [tipoPartitura]="_partitura"
  ></app-lista-partituras>`,
})
export class JesucristoComponent {
  public _query = 'jesucristo';
  public _navigateTo = 'canto-liturgico-y-religioso/partitura';
  public _titulo = 'CANTOS JESUCRISTO';
  public _partitura = 'CANTOS A JESUCRISTO';
}
