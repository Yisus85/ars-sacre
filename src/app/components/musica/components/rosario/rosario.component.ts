import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SeoService } from '../../../../../app/services/seo.service';

@Component({
  selector: 'app-rosario',
  templateUrl: './rosario.component.html',
  styleUrls: ['./rosario.component.scss']
})
export class RosarioComponent implements OnInit {

  constructor(private title: Title, private seo: SeoService) { }

  ngOnInit() {
    this.title.setTitle('Cantos para Rosario Ars-Sacre');
    this.seo.createTags({
      author: 'Jesus David D Leon',
      description: 'Encuentra partituras de cantos para el Rosario',
      slug: 'canto-liturgico-y-religioso/rosario',
      title: 'Cantos para Rosario Ars-Sacre',
    });
  }

}
