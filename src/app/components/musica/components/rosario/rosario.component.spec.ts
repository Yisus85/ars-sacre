import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RosarioComponent } from './rosario.component';

describe('RosarioComponent', () => {
  let component: RosarioComponent;
  let fixture: ComponentFixture<RosarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RosarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RosarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
