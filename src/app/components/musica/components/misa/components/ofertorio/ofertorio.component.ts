import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ofertorio',
  template: `<app-lista-partituras
    [query]='_query'
    [navigateTo]='_navigateTo'
    [headerTitulo]='_titulo'
    [tipoPartitura]="_partitura"
  >
  </app-lista-partituras>`,
})
export class OfertorioComponent {
  public _query = 'ofertorio';
  public _navigateTo = 'canto-liturgico-y-religioso/partitura';
  public _titulo = 'CANTOS DE OFERTORIO';
  public _partitura = 'CANTO';
}
