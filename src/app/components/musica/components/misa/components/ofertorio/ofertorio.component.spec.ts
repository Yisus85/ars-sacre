import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfertorioComponent } from './ofertorio.component';

describe('OfertorioComponent', () => {
  let component: OfertorioComponent;
  let fixture: ComponentFixture<OfertorioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfertorioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfertorioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
