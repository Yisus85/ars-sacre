import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotidianaComponent } from './cotidiana.component';

describe('CotidianaComponent', () => {
  let component: CotidianaComponent;
  let fixture: ComponentFixture<CotidianaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotidianaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotidianaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
