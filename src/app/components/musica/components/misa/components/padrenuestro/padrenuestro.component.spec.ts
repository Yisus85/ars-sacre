import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PadrenuestroComponent } from './padrenuestro.component';

describe('PadrenuestroComponent', () => {
  let component: PadrenuestroComponent;
  let fixture: ComponentFixture<PadrenuestroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PadrenuestroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PadrenuestroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
