import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-entrada',
  template: `<app-lista-partituras
    [query]='_query'
    [navigateTo]='_navigateTo'
    [headerTitulo]='_titulo'
    [tipoPartitura]="_partitura"
  >
  </app-lista-partituras>`,
})
export class EntradaComponent {
  public _query = 'entrada';
  public _navigateTo = 'canto-liturgico-y-religioso/partitura';
  public _titulo = 'CANTOS DE ENTRADA';
  public _partitura = 'CANTO';
}
