import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AleluyaComponent } from './aleluya.component';

describe('AleluyaComponent', () => {
  let component: AleluyaComponent;
  let fixture: ComponentFixture<AleluyaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AleluyaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AleluyaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
