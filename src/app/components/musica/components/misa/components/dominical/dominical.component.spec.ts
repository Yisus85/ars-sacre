import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DominicalComponent } from './dominical.component';

describe('DominicalComponent', () => {
  let component: DominicalComponent;
  let fixture: ComponentFixture<DominicalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DominicalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DominicalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
