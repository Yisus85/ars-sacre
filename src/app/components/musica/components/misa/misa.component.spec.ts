import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MisaComponent } from './misa.component';

describe('MisaComponent', () => {
  let component: MisaComponent;
  let fixture: ComponentFixture<MisaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MisaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
