import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SeoService } from '../../../../../app/services/seo.service';

@Component({
  selector: 'app-misa',
  templateUrl: './misa.component.html',
  styleUrls: ['./misa.component.scss']
})
export class MisaComponent implements OnInit {

  constructor(private title: Title, private seo: SeoService) { }

  ngOnInit() {
    this.title.setTitle('Cantos de Misa Ars-Sacre');
    this.seo.createTags({
      author: 'Jesus David D Leon',
      description: 'Encuentra partituras para misa',
      slug: 'canto-liturgico-y-religioso/misa',
      title: 'Ars-Sacre Cantos de Misa',
    });
  }

}
