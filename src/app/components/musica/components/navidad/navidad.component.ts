import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SeoService } from '../../../../../app/services/seo.service';

@Component({
  selector: 'app-navidad',
  templateUrl: './navidad.component.html',
  styleUrls: ['./navidad.component.scss']
})
export class NavidadComponent implements OnInit {

  constructor(private title: Title, private seo: SeoService) { }

  ngOnInit() {
    this.title.setTitle('Navidad Ars-Sacre');
    this.seo.createTags({
      author: 'Jesus David D Leon',
      description: 'Encuentra partituras de diversos cantos Navideños',
      slug: 'canto-liturgico-y-religioso/navidad',
      title: 'Navidad Ars-Sacre',
    });
  }

}
