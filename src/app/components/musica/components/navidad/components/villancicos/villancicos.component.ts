import { Component } from '@angular/core';

@Component({
  template: `<app-lista-partituras
    [query]='villancicoQuery'
    [navigateTo]='navigateToV'
    [headerTitulo]='tituloV'
    [tipoPartitura]="tipoPartitura"
  ></app-lista-partituras>`,
})
export class VillancicosComponent {
  public villancicoQuery = 'villancicos';
  public navigateToV = '/canto-liturgico-y-religioso/partitura';
  public tituloV = 'VILLANCICOS';
  public tipoPartitura = 'VILLANCICO';
}
