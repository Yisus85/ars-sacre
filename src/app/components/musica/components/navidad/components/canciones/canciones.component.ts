import { Component } from '@angular/core';

@Component({
  template: `<app-lista-partituras
    [query]='cancionQuery'
    [navigateTo]='navigateToCN'
    [headerTitulo]='tituloCN'
    [tipoPartitura]="tipoPartitura"
  ></app-lista-partituras>`,
})
export class CancionesComponent {
  public cancionQuery = 'cancionesNavidad';
  public navigateToCN = '/canto-liturgico-y-religioso/partitura';
  public tituloCN = 'CANCIONES DE NAVIDAD';
  public tipoPartitura = 'CANCION NAVIDEÑA';
}
