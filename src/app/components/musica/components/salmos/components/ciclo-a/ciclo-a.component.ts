import { Component } from '@angular/core';

@Component({
  selector: 'app-ciclo-a',
  template: `<app-lista-partituras
    [query]='cicloAquery'
    [navigateTo]='navigateToA'
    [headerTitulo]='tituloA'
    [tipoPartitura]="tipoPartitura"
  ></app-lista-partituras>`,
})
export class CicloAComponent {
  public cicloAquery = 'salmosA';
  public navigateToA = '/canto-liturgico-y-religioso/partitura';
  public tituloA = 'SALMOS CICLO A';
  public tipoPartitura = 'SALMO';
}
