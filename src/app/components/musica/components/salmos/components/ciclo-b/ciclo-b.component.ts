import { Component } from '@angular/core';

@Component({
  selector: 'app-ciclo-b',
  template: `
  <app-lista-partituras
    [query]='cicloBquery'
    [navigateTo]='navigateToB'
    [headerTitulo]='tituloB'
    [tipoPartitura]="tipoPartitura"
  ></app-lista-partituras>`,
})
export class CicloBComponent {
  public tipoPartitura = 'SALMO';
  public cicloBquery = 'salmosB';
  public navigateToB = '/canto-liturgico-y-religioso/partitura';
  public tituloB = 'SALMOS CICLO B';
}
