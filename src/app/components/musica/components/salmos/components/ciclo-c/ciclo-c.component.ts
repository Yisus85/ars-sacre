import { Component } from '@angular/core';

@Component({
  selector: 'app-ciclo-c',
  template: `<app-lista-partituras
    [query]='cicloCquery'
    [navigateTo]='navigateToC'
    [headerTitulo]='tituloC'
    [tipoPartitura]="tipoPartitura"
  ></app-lista-partituras>`,
})
export class CicloCComponent {
  public cicloCquery = 'salmosC';
  public navigateToC = '/canto-liturgico-y-religioso/partitura';
  public tituloC = 'SALMOS CICLO C';
  public tipoPartitura = 'SALMO';
}
