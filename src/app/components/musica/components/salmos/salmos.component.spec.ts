import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalmosComponent } from './salmos.component';

describe('SalmosComponent', () => {
  let component: SalmosComponent;
  let fixture: ComponentFixture<SalmosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalmosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalmosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
