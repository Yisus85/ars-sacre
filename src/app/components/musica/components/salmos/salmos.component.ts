import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SeoService } from '../../../../../app/services/seo.service';

@Component({
  selector: 'app-salmos',
  templateUrl: './salmos.component.html',
  styleUrls: ['./salmos.component.scss']
})
export class SalmosComponent implements OnInit {

  constructor(private title: Title, private seo: SeoService) { }

  ngOnInit() {
    this.title.setTitle('Salmos Ars-Sacre');
    this.seo.createTags({
      author: 'Jesus David D Leon',
      description: 'Encuentra partituras de diversos salmos',
      slug: 'canto-liturgico-y-religioso/salmos',
      title: 'Salmos Ars-Sacre',
    });
  }

}
