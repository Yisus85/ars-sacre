import { Component, OnInit, Renderer2, Inject, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { SeoService } from '../../../../../app/services/seo.service';
// import { DOCUMENT } from '@angular/platform-browser';

@Component({
  selector: 'app-home-musica',
  templateUrl: './home-musica.component.html',
  styleUrls: ['./home-musica.component.scss']
})
export class HomeMusicaComponent implements OnInit {
  @ViewChild('amazonAd') amazonAd;

  constructor(private title: Title, private seo: SeoService, private renderer: Renderer2, /*@Inject(DOCUMENT) private _document*/) { }

  ngOnInit() {
    this.title.setTitle('Ars-Sacre. Música');
    this.seo.createTags({
      author: 'Jesus David D Leon',
      description: 'Encuentra partituras de cantos liturgitos y religiosos',
      slug: 'canto-liturgico-y-religioso',
      title: 'Ars-Sacre Home',
    });
    const scriptAmazon = this.renderer.createElement('script');
    // tslint:disable-next-line:max-line-length
    scriptAmazon.src = '//z-na.amazon-adsystem.com/widgets/onejs?MarketPlace=US&adInstanceId=af1e999f-71d7-4f25-be0e-a6d41b479545';
    scriptAmazon.async = true;

    // this.renderer.appendChild(this._document.getElementById('amzn-assoc-ad-af1e999f-71d7-4f25-be0e-a6d41b479545'), scriptAmazon);
  }

}
