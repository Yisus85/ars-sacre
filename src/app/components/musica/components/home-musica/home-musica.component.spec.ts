import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeMusicaComponent } from './home-musica.component';

describe('HomeMusicaComponent', () => {
  let component: HomeMusicaComponent;
  let fixture: ComponentFixture<HomeMusicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeMusicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeMusicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
