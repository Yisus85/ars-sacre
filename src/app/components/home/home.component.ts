import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, HostListener, OnDestroy } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { SeoService } from '../../services/seo.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { PartiturasService } from '../../services/partituras.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public divWidth = 0;
  public imgFormat = 'webp';
  public imgSize = 'small';
  public totalParituras$ = this.partitura.totalParituras$.pipe(
    map(({data}) => data.totalCount),
  );
  public noFixedAttachement = false;
  @ViewChild('mainImageRef') mainImageRef: ElementRef;
  @HostListener('window:resize') onResize() {
    if (this.mainImageRef) {
      this.divWidth = this.mainImageRef.nativeElement.clientWidth;
      this.setImagesSize(this.divWidth);
    }
  }

  constructor(
    private router: Router,
    private matIconRegistry: MatIconRegistry,
    private domSanitizaer: DomSanitizer,
    private seo: SeoService,
    private title: Title,
    private deviceDetector: DeviceDetectorService,
    private partitura: PartiturasService,
  ) {
    this.matIconRegistry.addSvgIcon('teatro', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/theater.svg'));
    this.matIconRegistry.addSvgIcon('orquesta', this.domSanitizaer.bypassSecurityTrustResourceUrl('assets/icons/orquesta.svg'));
  }

  setImagesSize(width: number) {
    if (width <= 720) {
      this.imgSize = 'small';
    } else if (width > 720 && width <= 1024) {
      this.imgSize = 'medium';
    } else if (width > 1024) {
      this.imgSize = 'large';
    }
  }


  goToUrl(url): void {
    if (url) {
      this.router.navigateByUrl(`/${url}`);
    } else {
      return;
    }
  }

  ngOnInit() {
    this.noFixedAttachement = this.deviceDetector.isMobile() || this.deviceDetector.isTablet();
    this.title.setTitle('Ars-Sacre. Música religiosa');
    this.seo.createTags({
      author: 'Jesus David D Leon',
      // tslint:disable-next-line:max-line-length
      description: 'Partituras de Salmos, cantos para Misa, cantos para Rosario, Villancicos, Canciones navideñas, y más... perteneientes a la religión católica en formato PDF.',
      slug: 'home',
      title: 'Ars-Sacre. Partituras de música católica',
    });
  }
}
