import * as express from 'express';
import * as path from 'path';
import * as compression from 'compression';
import { server } from './configs/server.config';

const folderAngular = path.join(__dirname, '../browser');

server.use(compression());

server.use(express.static(folderAngular));

server.get('/*', (req, res) => {
    res.sendFile(path.join(folderAngular, 'index.html'));
}); // Redirect to Angular home
const app = server.listen(server.get('port'), function () {
    console.log('Serve running on ' + server.get('port'));
});
