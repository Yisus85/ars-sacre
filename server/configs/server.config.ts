import * as express from 'express';
import * as bodyParser from 'body-parser';

const server = express();

server.set('port', (process.env.PORT || 8000));
// server.set('view engine', 'html');
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: false }));

export { server };
